#!/usr/bin/make -f
# -*- makefile -*-

# This has to be exported to make some magic below work.
export DH_OPTIONS

JAVA_HOME?=/usr/lib/jvm/default-java
JFLAGS?=-source 1.8 -nowarn

SRCDIR := $(shell echo ./$(word $(words ${MAKEFILE_LIST}), ${MAKEFILE_LIST}) | \
sed -r "s/debian\/rules$$//")
VERSION := $(shell cd ${SRCDIR} && dpkg-parsechangelog | egrep '^Version:' | \
cut -f 2 -d ' ' | sed -r 's/\.dfsg(.)*//g')

NANOXML := nanoxml.jar
LITE := nanoxml-lite.jar
SAX := nanoxml-sax.jar

%:
	dh $@ --with javahelper

override_dh_auto_build:
	jh_build --javacopts='${JFLAGS}' --no-javadoc ${LITE} Sources/Lite/
	jh_build --javacopts='${JFLAGS}' --no-javadoc ${NANOXML} Sources/Java/
	CLASSPATH=${NANOXML} jh_build --javacopts='${JFLAGS}' --no-javadoc ${SAX} Sources/SAX/
	jh_manifest -c /usr/share/java/${NANOXML} ${SAX}

	bnd wrap --bsn net.n3.nanoxml      --output $(NANOXML).tmp $(NANOXML)
	bnd wrap --bsn net.n3.nanoxml.lite --output $(LITE).tmp $(LITE)
	bnd wrap --bsn net.n3.nanoxml.sax  --output $(SAX).tmp $(SAX)
	mv $(NANOXML).tmp $(NANOXML)
	mv $(LITE).tmp $(LITE)
	mv $(SAX).tmp $(SAX)

	${JAVA_HOME}/bin/javadoc -author -quiet -Xdoclint:none \
	-sourcepath Sources/Java/:Sources/Lite/:Sources/SAX/ -source 1.8 \
	nanoxml net.n3.nanoxml net.n3.nanoxml.sax -d api

override_dh_auto_test:
ifeq (,$(filter nocheck, $(DEB_BUILD_OPTIONS)))
	make -f debian/rules test
else
	echo "Disabling tests"
endif

override_dh_clean:
	jh_clean
	rm -rf Test/*/*.class
	rm -rf debian/orig.tmp || echo "No failed source fetch"

	dh_clean

test:
	set -e; \
	set -x; \
	cd Test/Lite && \
	${JAVA_HOME}/bin/javac ${JFLAGS} -cp .:../../${LITE} `find -iname *.java` && \
	for TESTFILE in *.xml; do \
		${JAVA_HOME}/bin/java -cp .:../../${LITE} DumpXML_Lite $${TESTFILE} > $${TESTFILE}.test_out ; \
		if ! diff -u -w $${TESTFILE}.out $${TESTFILE}.test_out ; \
		then	echo ${LITE} failed $${TESTFILE}; \
			exit 1; \
		fi; \
	done; \
	cd ../Java && \
	${JAVA_HOME}/bin/javac ${JFLAGS} -cp .:../../${NANOXML} $$(find -iname *.java) && \
	for TESTFILE in *.xml; do\
		${JAVA_HOME}/bin/java -cp .:../../${NANOXML} DumpXML $${TESTFILE} > $${TESTFILE}.test_out ; \
		if ! diff -u -w $${TESTFILE}.out $${TESTFILE}.test_out ; \
		then	echo ${NANOXML} failed $${TESTFILE}; \
			exit 1; \
		fi; \
	done
	touch $@

